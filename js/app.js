$(document).ready(function() {
 
   
    

    var ancho = $(window).width();
    if(ancho < 768){
        $.fn.fullpage.setResponsive(true);
    } else {
        $('#fullpage').fullpage({
            verticalCentered: true,
            navigation: true,
            offsetSections: true
        });
    }

    $(window).resize(function() {
        var ancho = $(window).width();
        if(ancho < 768){
            $.fn.fullpage.setResponsive(true);
        } else {
            $.fn.fullpage.setResponsive(false);
            $('#fullpage').fullpage({
                verticalCentered: true,
                navigation: true,
                offsetSections: true
            });
        }
    });

});
